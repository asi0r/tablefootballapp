import React, { Component } from 'react';
import "./MatchesList.scss";
import { Table, TableBody, TableRow, TableCell } from '@material-ui/core';
import { Match } from '../pages/Page';

class OwnProps {
  matches: Match[];
}

type Props = OwnProps;

class MatchesList extends Component<Props>{
  render() {
    return (
      <main className="matches-list">
        <section className='wrap-table'>
          <h1>Lista meczów</h1>
          <div className='rwd-table'>
            <Table className='table'>
              <TableBody className='table-body'>
                {this.props.matches.map(match => (
                  <TableRow className='table-row' key={match.id}>
                    <TableCell className='table-cell' component="th" scope="row">
                      {match.formatedDate}
                    </TableCell>
                    <TableCell className='table-cell' align="right">{match.winners.map(win => `${win.name} ${" "}`)}</TableCell>
                    <TableCell className='table-cell' align="right"><span>{`${match.scoreWin} : ${match.scoreLoss}`}</span></TableCell>
                    <TableCell className='table-cell' align="right">{match.loosers.map(loss => `${loss.name} ${" "}`)}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>
        </section>
      </main>
    )
  }
}

export default MatchesList;