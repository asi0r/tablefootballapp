import Button from "@material-ui/core/Button";
import Switch from "@material-ui/core/Switch";
import React, { Component } from "react";
import { RouteComponentProps, withRouter } from "react-router-dom";
import PlayerSelection from "./PlayerSelection";
import "./NewMatch.scss";
import { PlayersState } from "./NewMatch";
import { Player } from "../pages/Page";
import Notification from "../common/Notification";

class State {
  matchFormula?: MatchFormulaType;
  players: Player[];
  playerOne?: Player;
  playerTwo?: Player;
  playerOneA?: Player;
  playerTwoA?: Player;
  playerOneB?: Player;
  playerTwoB?: Player;
  pickedPlayers: Player[];
  pickedPlayersAB: Player[];
  playerSelectionOn: boolean;
  showNotification: boolean;

}
enum MatchFormulaType {
  OneOnOne,
  TwoOnTwo
}

class OwnProps {
  players: Player[];
  playersState: (playersState: PlayersState) => void;
}

type Props = OwnProps & RouteComponentProps;

class PickTeam extends Component<Props, State> {

  constructor(props: Props) {
    super(props);
    this.state = {
      matchFormula: undefined,
      players: this.props.players,
      playerOne: undefined,
      playerTwo: undefined,
      playerOneA: undefined,
      playerOneB: undefined,
      playerTwoA: undefined,
      playerTwoB: undefined,
      pickedPlayers: [],
      pickedPlayersAB: [],
      playerSelectionOn: false,
      showNotification: false,
    };
  }
  isNotificationShow = (status: boolean) => {
    this.setState({ showNotification: status })
  }

  handleChangePlayer = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => {
    const player = this.state.players.find(
      p => p.id === +event.target.value
    ) as Player;
    let players = this.state.players;

    if (event.target.name === "playerOne") {
      await this.setState({
        playerOne: player,
        players
      });
    } else {
      await this.setState({
        playerTwo: player,
        players
      });
    }

    let pickedPlayers: Player[] = [];

    this.state.playerOne as Player !== undefined && pickedPlayers.push(this.state.playerOne as Player);
    this.state.playerTwo as Player !== undefined && pickedPlayers.push(this.state.playerTwo as Player);
    await this.setState({ pickedPlayers });
  };

  handleChangePlayer2to2 = async (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => {
    const player = this.state.players.find(
      p => p.id === +event.target.value
    ) as Player;
    let players = this.state.players;

    if (event.target.name === "playerOneA") {
      await this.setState({
        playerOneA: player,
        players
      });
    } else if (event.target.name === "playerOneB") {
      await this.setState({
        playerOneB: player,
        players
      });
    } else if (event.target.name === "playerTwoA") {
      await this.setState({
        playerTwoA: player,
        players
      });
    } else if (event.target.name === "playerTwoB") {
      await this.setState({
        playerTwoB: player,
        players
      });
    }

    let pickedPlayersAB: Player[] = [];

    this.state.playerOneA as Player !== undefined && pickedPlayersAB.push(this.state.playerOneA as Player);
    this.state.playerOneB as Player !== undefined && pickedPlayersAB.push(this.state.playerOneB as Player);
    this.state.playerTwoA as Player !== undefined && pickedPlayersAB.push(this.state.playerTwoA as Player);
    this.state.playerTwoB as Player !== undefined && pickedPlayersAB.push(this.state.playerTwoB as Player);
    await this.setState({ pickedPlayersAB });
  }

  handlePlayerSelectionOn = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    this.setState({ playerSelectionOn: true })
  }

  handleRandomTeam = async (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    const players = this.state.players;
    const playersSelect: number[] = [];
    const matchFormula = this.state.matchFormula;
    if (matchFormula === 0) {
      for (; playersSelect.length < 2;) {
        const playerIndex = Math.floor(Math.random() * players.length) + 1;
        if (!playersSelect.includes(playerIndex)) {
          playersSelect.push(playerIndex)
        }
      }

      const playerOne = this.state.players.find(p => p.id === playersSelect[0]);
      const playerTwo = this.state.players.find(p => p.id === playersSelect[1]);

      this.setState({
        playerOne,
        playerTwo
      });

      let pickedPlayers: Player[] = [];
      pickedPlayers.push(playerOne as Player);
      pickedPlayers.push(playerTwo as Player);
      await this.setState({ pickedPlayers });
    }
    else {
      for (; playersSelect.length < 4;) {
        const playerIndex = Math.floor(Math.random() * players.length) + 1;
        if (!playersSelect.includes(playerIndex)) {
          playersSelect.push(playerIndex)
        }
      }
      const playerOneA = this.state.players.find(p => p.id === playersSelect[0]);
      const playerOneB = this.state.players.find(p => p.id === playersSelect[2]);
      const playerTwoA = this.state.players.find(p => p.id === playersSelect[1]);
      const playerTwoB = this.state.players.find(p => p.id === playersSelect[3]);

      this.setState({
        playerOneA,
        playerOneB,
        playerTwoA,
        playerTwoB,
      });

      let pickedPlayersAB: Player[] = [];
      pickedPlayersAB.push(playerOneA as Player);
      pickedPlayersAB.push(playerOneB as Player);
      pickedPlayersAB.push(playerTwoA as Player);
      pickedPlayersAB.push(playerTwoB as Player);
      await this.setState({ pickedPlayersAB });
    }
    this.setState({ playerSelectionOn: true })
  }

  handlePlay = () => {
    const { playerOne, playerTwo, playerOneA, playerTwoA, playerOneB, playerTwoB, matchFormula } = this.state;
    if (matchFormula === 0 && (playerOne === undefined || playerTwo === undefined)) {
      this.setState({ showNotification: true })
    }
    else if (matchFormula === 1 && (playerOneA === undefined || playerOneB === undefined || playerTwoA === undefined || playerTwoB === undefined)) {
      this.setState({ showNotification: true })
    }
    else {
      this.props.history.push("/match/result");
      let playersState: PlayersState = { ...this.state }
      this.props.playersState(playersState);
    }
  }

  handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ showNotification: false });
  }

  handleChange(e: React.ChangeEvent<HTMLInputElement>, c: boolean) {
    this.setState({ matchFormula: +e.target.value });
  }
  render() {
    const playersList = this.state.players.map(player =>
      this.state.pickedPlayers.includes(player) ? (
        <option value={player.id} disabled key={player.id}>
          {player.name}
        </option>
      ) : (
          <option key={player.id} value={player.id}>
            {player.name}
          </option>
        )
    );

    const playersList2to2 = this.state.players.map(player =>
      this.state.pickedPlayersAB.includes(player) ? (
        <option value={player.id} disabled key={player.id}>
          {player.name}
        </option>
      ) : (
          <option key={player.id} value={player.id}>
            {player.name}
          </option>
        )
    );
    return (
      <main className="new-match">
        <div className="wrap">
          <h1>Nowy mecz</h1>
          <div className="desktop-view clearfix">
            <section className="number-of-players">
              <span>
                {" "}
                1 na 1
              <Switch
                  className="switch"
                  onChange={(e, c) => this.handleChange(e, c)}
                  checked={this.state.matchFormula === MatchFormulaType.OneOnOne}
                  value={0}
                />
              </span>
              <span>
                {" "}
                2 na 2
              <Switch
                  className="switch"
                  checked={this.state.matchFormula === MatchFormulaType.TwoOnTwo}
                  onChange={(e, c) => this.handleChange(e, c)}
                  value={1}
                  color="primary"
                />
              </span>
            </section>
            <section className="buttons">
              <Button
                disabled={this.state.matchFormula === undefined}
                variant="contained"
                color="primary"
                className="button"
                onClick={this.handleRandomTeam}
              >
                Losuj drużynę
            </Button>
              {!this.state.playerSelectionOn &&
                (<Button
                  disabled={this.state.matchFormula === undefined}
                  variant="contained"
                  color="primary"
                  className="button"
                  onClick={this.handlePlayerSelectionOn}
                >
                  wybierz skład
            </Button>)
              }
            </section>
          </div>

          {this.state.playerSelectionOn
            ?
            (this.state.matchFormula !== undefined &&
              this.state.matchFormula === MatchFormulaType.OneOnOne && (
                <section className="one-to-one">
                  <PlayerSelection
                    value={(this.state.playerOne as Player) && (this.state.playerOne as Player).id}
                    name="playerOne"
                    text="Wybierz I gracza"
                    playersList={playersList}
                    onChange={async e => await this.handleChangePlayer(e)}
                  />
                  <PlayerSelection
                    value={(this.state.playerTwo as Player) && (this.state.playerTwo as Player).id}
                    name="playerTwo"
                    text="Wybierz II gracza"
                    playersList={playersList}
                    onChange={async e => await this.handleChangePlayer(e)}
                  />
                  <Button
                    disabled={this.state.matchFormula === undefined}
                    variant="contained"
                    color="primary"
                    className="button"
                    onClick={this.handlePlay}
                  >
                    Graj
            </Button>
                </section>
              ))
            :
            null}
          {this.state.playerSelectionOn
            ?
            (this.state.matchFormula !== undefined &&
              this.state.matchFormula === MatchFormulaType.TwoOnTwo && (
                <section className="two-to-two">
                  <div className="team">
                    <h2>Drużyna A</h2>
                    <PlayerSelection
                      value={(this.state.playerOneA as Player) && (this.state.playerOneA as Player).id}
                      name="playerOneA"
                      text="Wybierz I gracza"
                      playersList={playersList2to2}
                      onChange={async e => await this.handleChangePlayer2to2(e)}
                    />
                    <PlayerSelection
                      value={(this.state.playerOneB as Player) && (this.state.playerOneB as Player).id}
                      name="playerOneB"
                      text="Wybierz II gracza"
                      playersList={playersList2to2}
                      onChange={async e => await this.handleChangePlayer2to2(e)}
                    />
                  </div>
                  <div className="team">
                    <h2>Drużyna B </h2>
                    <PlayerSelection
                      value={(this.state.playerTwoA as Player) && (this.state.playerTwoA as Player).id}
                      name="playerTwoA"
                      text="Wybierz I gracza"
                      playersList={playersList2to2}
                      onChange={async e => await this.handleChangePlayer2to2(e)}
                    />
                    <PlayerSelection
                      value={(this.state.playerTwoB as Player) && (this.state.playerTwoB as Player).id}
                      name="playerTwoB"
                      text="Wybierz II gracza"
                      playersList={playersList2to2}
                      onChange={async e => await this.handleChangePlayer2to2(e)}
                    />
                  </div>
                  <Button
                    disabled={this.state.matchFormula === undefined}
                    variant="contained"
                    color="primary"
                    className="button"
                    onClick={this.handlePlay}
                  > Graj
            </Button>
                </section>
              ))
            : null}
        </div>
        <Notification text='Uzupenij wszystkie pola' duration={3000} open={this.state.showNotification} isNotificationShow={this.isNotificationShow} />
      </main>
    );
  }
}

export default withRouter(PickTeam);
