import React, { Component } from 'react';
import { Typography, InputLabel, InputBase, withStyles, FormControl, Button } from '@material-ui/core';
import { PlayersState, MatchFormulaType } from './NewMatch';
import { Player, Match } from '../pages/Page';
import "./MatchResult.scss";
import { withRouter, RouteComponentProps } from 'react-router';
import Notification from "../common/Notification";

class OwnProps {
  stateValue: PlayersState;
  addMatch: (match: Match) => void;
}
type Props = OwnProps & RouteComponentProps;

const BootstrapInput = withStyles(theme => ({
  root: {
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    width: 'auto',
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase);

class MatchResult extends Component<Props> {
  state = {
    scoreA: '',
    scoreB: '',
    showNotification: false,
  }

  isNotificationShow = (status: boolean) => {
    this.setState({ showNotification: status })
  }

  writeScore = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.name === 'scoreA') {
      this.setState({ scoreA: e.target.value });
    } else {
      this.setState({ scoreB: e.target.value });
    }
  }

  handleAddMatch = () => {
    let today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    let formatedDate = dd + '/' + mm + '/' + yyyy;

    const matchState = this.props.stateValue;
    if (this.props.stateValue.matchFormula === MatchFormulaType.OneOnOne) {
      const match: Match = {
        winners: +this.state.scoreA > +this.state.scoreB ? [matchState.playerOne as Player] : [matchState.playerTwo as Player],
        loosers: +this.state.scoreA < +this.state.scoreB ? [matchState.playerOne as Player] : [matchState.playerTwo as Player],
        scoreWin: +this.state.scoreA > +this.state.scoreB ? +this.state.scoreA : +this.state.scoreB,
        scoreLoss: +this.state.scoreA < +this.state.scoreB ? +this.state.scoreA : +this.state.scoreB,
        formatedDate: formatedDate,
        date: new Date()
      }
      if (this.state.scoreA === this.state.scoreB && this.state.scoreA === '' && this.state.scoreB === '') {
        this.setState({ showNotification: true })
      }
      else if (this.state.scoreA === this.state.scoreB && this.state.scoreA !== '' && this.state.scoreB !== '') {
        this.setState({ showNotification: true })
      }
      else {
        this.props.addMatch(match);
      }
    }
    else {
      const match: Match = {
        winners: +this.state.scoreA > +this.state.scoreB ? [matchState.playerOneA as Player, matchState.playerOneB as Player] : [matchState.playerTwoA as Player, matchState.playerTwoB as Player],
        loosers: +this.state.scoreA < +this.state.scoreB ? [matchState.playerOneA as Player, matchState.playerOneB as Player] : [matchState.playerTwoA as Player, matchState.playerTwoB as Player],
        scoreWin: +this.state.scoreA > +this.state.scoreB ? +this.state.scoreA : +this.state.scoreB,
        scoreLoss: +this.state.scoreA < +this.state.scoreB ? +this.state.scoreA : +this.state.scoreB,
        formatedDate: formatedDate,
        date: new Date()
      }
      this.props.addMatch(match);
    }
    if (this.state.scoreA === this.state.scoreB && this.state.scoreA === '' && this.state.scoreB === '') {
      this.setState({ showNotification: true })
    }
    else if (this.state.scoreA === this.state.scoreB && this.state.scoreA !== '' && this.state.scoreB !== '') {
      this.setState({ showNotification: true })
    }
    else this.props.history.push("/home");
  }

  handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ showNotification: false });
  }

  render() {
    return (
      <div className='match-result'>
        <div className='wrap'>
          <h1>Wynik meczu</h1>
          <section className='players-name'>
            <Typography className='name ' variant="h6" color="inherit">
              {this.props.stateValue.matchFormula === 0 ? (this.props.stateValue.playerOne as Player).name : (this.props.stateValue.playerOneA as Player).name + ' - ' + (this.props.stateValue.playerOneB as Player).name}
            </Typography>
            <Typography className='name' variant="h6" color="inherit">
              {this.props.stateValue.matchFormula === 0 ? (this.props.stateValue.playerTwo as Player).name : (this.props.stateValue.playerTwoA as Player).name + ' - ' + (this.props.stateValue.playerTwoB as Player).name}
            </Typography>
          </section>
          <FormControl className='form-control'>
            <InputLabel htmlFor="age-customized-select" className='input-label'>
              Wynik
          </InputLabel>
            <BootstrapInput className='input-score clearfix' name='scoreA' value={this.state.scoreA} onChange={(e: React.ChangeEvent<HTMLInputElement>) => this.writeScore(e)} />
          </FormControl>
          <span className='colon'>:</span>
          <FormControl className='form-control' >
            <InputLabel htmlFor="age-customized-select" className='input-label' >
              Wynik
          </InputLabel>
            <BootstrapInput className='input-score clearfix' name='scoreB' value={this.state.scoreB} onChange={(e: React.ChangeEvent<HTMLInputElement>) => this.writeScore(e)} />
          </FormControl>
          <Button variant="contained" color="primary" className='btn-add-match' onClick={this.handleAddMatch}>
            Dodaj mecz
      </Button>
          {(this.state.scoreA === this.state.scoreB && this.state.scoreA !== '' && this.state.scoreB !== '') ? <Notification text='Remis nie jest możliwy, sprawdź wynik' duration={3000} open={this.state.showNotification} isNotificationShow={this.isNotificationShow} /> : <Notification text='Uzupełnij wszystkie pola' duration={3000} open={this.state.showNotification} isNotificationShow={this.isNotificationShow} />}
        </div>
      </div>
    );
  }
}

export default withRouter(MatchResult);