import React, { Component } from 'react'
import PickTeam from './PickTeam';
import { Player, Match, MatchesTable } from '../pages/Page';
import { Route, Redirect, Switch } from 'react-router';
import MatchResult from './MatchResult';

export interface PlayersState {
  matchFormula?: MatchFormulaType;
  players: Player[];
  playerOne?: Player;
  playerTwo?: Player;
  playerOneA?: Player;
  playerTwoA?: Player;
  playerOneB?: Player;
  playerTwoB?: Player;
}

class State {
  playersState?: PlayersState;
}

export enum MatchFormulaType {
  OneOnOne,
  TwoOnTwo
}

class OwnProps {
  players: Player[];
  addMatch: (match: Match) => void;
  calculateTable: (table: MatchesTable) => void;
}

type Props = OwnProps;

class NewMatch extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      playersState: undefined
    };
  }

  saveState = (playersState: PlayersState) => {
    this.setState({ playersState });
  }

  render() {
    return (
      <Switch>
        <Redirect exact={true} from="/match" to="/match/pick-team" />
        <Route path="/match/result" render={() => <MatchResult stateValue={this.state.playersState as PlayersState} addMatch={this.props.addMatch} />} />
        <Route path="/match/pick-team" render={() => <PickTeam players={this.props.players} playersState={this.saveState} />} />
      </Switch>

    )
  }
}

export default NewMatch;