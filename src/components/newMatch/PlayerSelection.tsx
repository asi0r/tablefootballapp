import React from 'react';
import FormControl from "@material-ui/core/FormControl/FormControl";
import { NativeSelect, FormHelperText } from "@material-ui/core";

class OwnProps {
  name: string;
  playersList: {};
  text: string;
  value: number | undefined;
  onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void
}

const PlayerSelection: React.SFC<OwnProps> = (props) => {
  return (
    <div className="player-select">
      <FormControl className="select-form" style={{ minWidth: 20 }}>
        <NativeSelect
          className="player-selection"
          value={props.value}
          name={props.name}
          onChange={e => props.onChange(e)}
        >
          <option value="" />
          {props.playersList}
        </NativeSelect>
        <FormHelperText className="text">
          {props.text}
        </FormHelperText>
      </FormControl>
    </div>
  )
}
export default PlayerSelection;

