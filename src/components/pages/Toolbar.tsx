import React, { Component } from "react";
import "./Toolbar.scss";
import AppBar from "@material-ui/core/AppBar";
import {
  Toolbar as MaterialToolbar,
  Drawer,
  Divider,
  List,
  ListItem,
  ListItemText
} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Tab from "@material-ui/core/Tab";
import logo from "../../img/soccer-ball.png";
import {
  RouteComponentProps,
  withRouter,
  NavLink
} from "react-router-dom";

class RouterProps {
  section?: string;
}
type Props = RouteComponentProps<RouterProps>;

class Toolbar extends Component<Props> {
  state = {
    open: false,
    value: 0
  };

  componentWillReceiveProps(newProps: Props) {
    if (this.props.match.params.section !== newProps.match.params.section) {
      this.handleDrawerClose();
    }
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };
  handleDrawerClose = () => {
    this.setState({ open: false });
  };
  handleChange = (event: React.ChangeEvent<{}>, value: number) => {
    this.setState({ value });
    this.setState({ open: false });

  };
  redirectTo = (direction: string) => {
    this.props.history.push(direction);
  };

  render() {
    return (
      <div className="nav-bar">
        <AppBar className="app-bar" position="static">
          <MaterialToolbar>
            <IconButton
              onClick={this.handleDrawerOpen}
              className="icon-button"
              color="inherit"
              aria-label="Menu"
            >
              <MenuIcon className="icon" />
            </IconButton>
            <div className="clearfix title-icon">
              <img className="ball" src={logo} alt="ball" />
              <Typography className="name" variant="h6" color="inherit">
                Piłkarzyki
              </Typography>
            </div>
            <div className="menu-desktop">
              <ul className="menu">
                <li
                >
                  <NavLink to="/home"><Tab className="tab-item" label="Start" /></NavLink>
                </li>
                <li>
                  <NavLink to="/players"><Tab className="tab-item" label="Lista graczy" /></NavLink>
                </li>
                <li>
                  <NavLink to="/matches"><Tab className="tab-item" label="Lista meczów" /></NavLink>
                </li>
                <li>
                  <NavLink to="/match"><Tab className="tab-item" label="Nowy mecz" /></NavLink>
                </li>
                <li>
                  <NavLink to="/statistics"><Tab className="tab-item" label="Statystyki" /></NavLink>
                </li>
              </ul>
            </div>
          </MaterialToolbar>
        </AppBar>
        <Drawer
          className="drawer"
          variant="persistent"
          anchor="left"
          open={this.state.open}
          onClose={this.handleDrawerClose}
        >
          <div className="menu-close-item">
            <IconButton className="left-icon" onClick={this.handleDrawerClose}>
              <ChevronLeftIcon className="icon" />
            </IconButton>
          </div>
          <List component="nav" className="nav">
            <ListItem button className="menu-item">
              <NavLink to="/home"><ListItemText primary="Start" /></NavLink>
            </ListItem>
            <ListItem button className="menu-item">
              <NavLink to="/players"><ListItemText primary="Lista graczy" /></NavLink>
            </ListItem>
            <ListItem button className="menu-item">
              <NavLink to="/matches"><ListItemText primary="Lista meczów" /></NavLink>
            </ListItem>
            <ListItem button className="menu-item">
              <NavLink to="/match"><ListItemText primary="Nowy mecz" /></NavLink>
            </ListItem>
            <ListItem button className="menu-item">
              <NavLink to="/statistics"><ListItemText primary="Statystyki" /></NavLink>
            </ListItem>
          </List>
          <Divider className="divider" />
        </Drawer>
      </div>
    );
  }
}

export default withRouter(Toolbar);
