import React, { Component } from "react";
import { Route, Switch, Redirect } from "react-router";
import MainSite from "../home/MainSite";
import PlayerList from "../players/PlayerList";
import Statistics from "../statistics/Statistics";
import MatchesList from "../matches/MatchesList";
import NewMatch from "../newMatch/NewMatch";

export interface Player {
  id: number;
  name: string;
  avatar: string
}

export interface Match {
  winners: Player[];
  loosers: Player[];
  scoreWin: number;
  scoreLoss: number;
  date: Date;
  formatedDate: string;
  id?: number;
}

export interface MatchesTable {
  name: string;
  matchesCount: number;
  winCount: number;
  lossCount: number;
  winPercentage: number;
}

class State {
  players: Player[];
  matches: Match[];
  playersTable: MatchesTable[]
}

type Props = {};

class Page extends Component<Props, State>{
  constructor(props: Props) {
    super(props);
    this.state = {
      players: [
        { id: 1, name: "Bartosz", avatar: require('../../img/avatars/bartek.png') },
        { id: 2, name: "Wojtek", avatar: require('../../img/avatars/wojtek.png') },
        { id: 3, name: "Sylwia", avatar: require('../../img/avatars/sylwia.png') },
        { id: 4, name: "Adrian", avatar: require('../../img/avatars/adrian.png') },
        { id: 5, name: "Mikołaj", avatar: require('../../img/avatars/mikolaj.png') }
      ],
      matches: [],
      playersTable: []
    }
  }

  playersSort = () => {
    let players = this.state.players;
    players.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0));
    this.setState({ players });
  }

  addNewPlayer = (player: Player) => {
    let players = this.state.players.concat(player);
    this.playersSort()
    this.setState({ players })
  }

  removePlayer = (playerIndex: number) => {
    let players = this.state.players;
    players.splice(playerIndex, 1);
    this.setState({ players })
  }

  editPlayers = (player: Player) => {
    let players = this.state.players;
    const playerIndex = this.state.players.findIndex(p => p.id === player.id);
    players.splice(playerIndex, 1, player);
    this.setState({ players })
  }

  addMatch = (match: Match) => {
    match = this.addId(match);
    this.state.matches.push(match);
    this.setState({ matches: this.state.matches });
    this.calculateTable();
  }

  calculateTable = () => {
    const { players, matches } = this.state;

    let playersTable: MatchesTable[] = players.map(p => {
      let playerWins = 0;
      let playerLosses = 0;
      matches.forEach(match => {
        if (match.winners.includes(p)) {
          playerWins++
        }
        else if (match.loosers.includes(p)) {
          playerLosses++
        }
      })
      const matchesCount = playerLosses + playerWins;
      let winPercentage = Math.floor((playerWins *100 / matchesCount));
      if (playerWins === 0 || matchesCount === 0) {
        winPercentage = 0;
      }
      return { name: p.name, winCount: playerWins, lossCount: playerLosses, matchesCount, winPercentage } as MatchesTable
    })
    this.setState({ playersTable });
  }

  private addId = (match: Match): Match => {
    if (this.state.matches.length === 0) {
      return { id: 1, ...match }
    }
    return { id: this.state.matches.length + 1, ...match }
  }

  render() {
    return (
      <>
        <Switch>
          <Redirect exact={true} from="/" to="/home" />
          <Route path="/home" render={() => <MainSite players={this.state.players} addMatch={this.addMatch} playersTable={this.state.playersTable} />} />
          <Route path="/players" render={() => <PlayerList players={this.state.players} addNewPlayer={this.addNewPlayer} removePlayer={this.removePlayer} editPlayers={this.editPlayers} />} />
          <Route path="/matches" render={() => <MatchesList matches={this.state.matches} />} />
          <Route path="/match" render={() => <NewMatch players={this.state.players} addMatch={this.addMatch} calculateTable={this.calculateTable} />} />
          <Route path="/statistics" component={Statistics} />
        </Switch>
      </>
    );
  }
};

export default Page;