import React, { Component, ChangeEvent } from "react";
import "./PlayerList.scss";
import { Player } from "../pages/Page";
import {
  Grid,
  Typography,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  SwipeableDrawer,
  TextField,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Fab from "@material-ui/core/Fab";
import { Delete, Edit } from "@material-ui/icons/";

class OwnProps {
  players: Player[];
  addNewPlayer: (player: Player) => void;
  removePlayer: (playerIndex: number) => void;
  editPlayers: (player: Player) => void;
}

type Props = OwnProps;

class State {
  addNewPlayerPanel: boolean;
  name: string;
  avatar: string;
  removePlayersOff: boolean;
  idOfEditingPlayer?: number;
  editingPlayer?: Player;
  editPanel: boolean;
}

class PlayerList extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      addNewPlayerPanel: false,
      name: "",
      avatar: "",
      removePlayersOff: false,
      idOfEditingPlayer: undefined,
      editingPlayer: undefined,
      editPanel: false,
    };
  }

  handleAddNewPlayer = (event: React.MouseEvent<HTMLElement, MouseEvent>) => { };
  toggleDrawer = (toggle: boolean) => {
    this.setState({
      addNewPlayerPanel: toggle
    });
  };

  handleChange = (e: ChangeEvent<HTMLInputElement>, name: string) => {
    if (e.target.name === "name") {
      this.setState({ name: e.target.value });
    } else {
      this.setState({ avatar: e.target.value });
    }
  };

  backToPlayersList = () => {
    this.setState({ addNewPlayerPanel: false });
  };

  addToPlayers = () => {
    let name = this.state.name;
    name = name.charAt(0).toUpperCase() + name.slice(1);
    const avatar = this.state.avatar;
    const player: Player = { id: this.props.players.length + 1, name, avatar };
    this.props.addNewPlayer(player);
    this.setState({
      name: "",
      avatar: "",
      addNewPlayerPanel: false
    });
  };

  handleRemovePlayerOn = () => {
    this.setState({ removePlayersOff: !this.state.removePlayersOff });
  };

  handleRemovePlayer = (
    e: React.MouseEvent<HTMLElement, MouseEvent>,
    id: number
  ) => {
    const players = this.props.players;
    let playerIndex = players.findIndex(p => p.id === id);
    this.props.removePlayer(playerIndex);
  };

  editPlayer = () => {
    let player: Player = { id: this.state.idOfEditingPlayer as number, name: this.state.name, avatar: this.state.avatar }
    this.props.editPlayers(player)
    this.setState({
      editPanel: false,
      name: "",
      avatar: "",
    });
  };

  handleEdit = (e: React.MouseEvent<HTMLElement, MouseEvent>, id: number) => {
    let players = this.props.players;
    const player = players.find(p => p.id === id);

    player !== undefined && this.setState({
      editPanel: true,
      name: player.name,
      avatar: player.avatar,
      idOfEditingPlayer: player.id,
    });
  };

  render() {
    return (
      <main className="player-list-content">
        <div className="player-list">
          <h1>Lista graczy</h1>
          {this.props.players.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0)).map(p => (
            <Grid container spacing={16} className="grid">
              <Grid item xs={12} md={6}>
                <Typography variant="h6" className="">
                </Typography>
                <div className="players">
                  <List className="list">
                    <ListItem className="list-item">
                      <ListItemAvatar>
                        <Avatar className="avatar" src={p.avatar} />
                      </ListItemAvatar>
                      <ListItemText
                        className="player-name"
                        primary={p.name}
                      />
                      <Fab
                        color="primary"
                        aria-label="Edit"
                        className="edit-icon"
                        onClick={(
                          e: React.MouseEvent<HTMLElement, MouseEvent>
                        ) => this.handleEdit(e, p.id)}
                      >
                        <Edit />
                      </Fab>
                      {this.state.removePlayersOff === true && (
                        <Fab
                          name={`name${p.id}`}
                          onClick={(
                            e: React.MouseEvent<HTMLElement, MouseEvent>
                          ) => this.handleRemovePlayer(e, p.id)}
                          aria-label="Delete"
                          className="delete-icon"
                        >
                          <Delete />
                        </Fab>
                      )}
                    </ListItem>
                  </List>
                </div>
              </Grid>
            </Grid>
          ))}
          <SwipeableDrawer
            className="right-panel"
            anchor="right"
            open={this.state.addNewPlayerPanel}
            onClose={(e: React.SyntheticEvent<{}, Event>) =>
              this.toggleDrawer(false)
            }
            onOpen={(e: React.SyntheticEvent<{}, Event>) =>
              this.toggleDrawer(true)
            }
          >
            <form
              className="add-new-player-panel"
              noValidate
              autoComplete="off"
            >
              <TextField
                className="name"
                name="name"
                label="Imię gracza"
                value={this.state.name}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  this.handleChange(e, "name")
                }
                margin="normal"
                variant="outlined"
              />
              <TextField
                className="avatar"
                name="avatar"
                label="Avatar"
                value={this.state.avatar}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  this.handleChange(e, "avatar")
                }
                margin="normal"
                variant="outlined"
              />
              <Button
                variant="outlined"
                color="primary"
                className="button-back"
                onClick={this.backToPlayersList}
              >
                Powrót
              </Button>
              <Button
                variant="outlined"
                color="primary"
                className="button-add"
                disabled={this.state.name === "" && this.state.avatar === ""}
                onClick={() => this.addToPlayers()}
              >
                Dodaj
                </Button>
            </form>
          </SwipeableDrawer>

          <SwipeableDrawer
            className="right-panel"
            anchor="right"
            open={this.state.editPanel}
            onClose={(e: React.SyntheticEvent<{}, Event>) =>
              this.toggleDrawer(false)
            }
            onOpen={(e: React.SyntheticEvent<{}, Event>) =>
              this.toggleDrawer(true)
            }
          >
            <form
              className="add-new-player-panel"
              noValidate
              autoComplete="off"
            >
              <TextField
                className="name"
                name="name"
                label="Imię gracza"
                value={this.state.name}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  this.handleChange(e, "name")
                }
                margin="normal"
                variant="outlined"
              />
              <TextField
                className="avatar"
                name="avatar"
                label="Avatar"
                value={this.state.avatar}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  this.handleChange(e, "avatar")
                }
                margin="normal"
                variant="outlined"
              />
              <Button
                variant="outlined"
                color="primary"
                className="button-back"
                onClick={this.backToPlayersList}
              >
                Powrót
              </Button>
              <Button
                variant="outlined"
                color="primary"
                className="button-add"
                disabled={this.state.name === "" && this.state.avatar === ""}
                onClick={() => this.editPlayer()}
              // onClick={() => this.addToPlayers()}
              >
                Zapisz
                </Button>
            </form>
          </SwipeableDrawer>

          <Button
            variant="contained"
            color="primary"
            className="add-player-button"
            onClick={(event: React.MouseEvent<HTMLElement, MouseEvent>) =>
              this.toggleDrawer(true)
            }
          >
            Dodaj gracza
          </Button>
          <Button
            variant="contained"
            color="secondary"
            className="remove-player-button"
            onClick={this.handleRemovePlayerOn}
          >
            {this.state.removePlayersOff === true ? "Powrót" : "Usuń gracza"}
          </Button>
        </div>
      </main>
    );
  }
}

export default PlayerList;
