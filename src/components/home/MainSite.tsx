import React, { Component } from 'react';
import { Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { Player, Match, MatchesTable } from '../pages/Page';
import "../styles/MainSite.scss";
class OwnProps {
  players: Player[];
  addMatch: (match: Match) => void;
  playersTable: MatchesTable[];
}

type Props = OwnProps;

class MainSite extends Component<Props>{
  render() {
    return (
      <main className='main-site'>
        <section className="table-wrap">
          <h1>Tabela wyników</h1>
          <div className='rwd-table'>
            <Table className='table'>
              <TableHead className='table-head'>
                <TableRow className='table-row'>
                  <TableCell className='table-cell'>Gracz</TableCell>
                  <TableCell className='table-cell' align="right">Liczba rozegranych meczów</TableCell>
                  <TableCell className='table-cell' align="right">Liczba zwycięstw</TableCell>
                  <TableCell className='table-cell' align="right">Liczba porażek</TableCell>
                  <TableCell className='table-cell' align="right">Stosunek porażek do wszystkich meczów (%)</TableCell>
                </TableRow>
              </TableHead>
              <TableBody className='table-body'>
                {this.props.playersTable.sort((a, b) => (a.winPercentage < b.winPercentage) ? 1 : ((a.winPercentage > b.winPercentage) ? -1 : 0)).map(player => (
                  <TableRow className='table-row' key={player.name}>
                    <TableCell className='table-cell' component="th" scope="row">
                      {player.name}
                    </TableCell>
                    <TableCell className='table-cell' align="right">{player.matchesCount}</TableCell>
                    <TableCell className='table-cell' align="right">{player.winCount}</TableCell>
                    <TableCell className='table-cell' align="right">{player.lossCount}</TableCell>
                    <TableCell className='table-cell' align="right">{player.winPercentage}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </div>
        </section>
      </main>
    )
  }
}

export default MainSite;