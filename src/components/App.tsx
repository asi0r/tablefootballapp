import React, { Component } from "react";
import "./App.css";
import Toolbar from "./pages/Toolbar";
import Page from "./pages/Page";

class App extends Component {
  render() {
    return (
        <div className="App">
          <header><Toolbar /></header>
          <main>
            <section className="pages"><Page /></section>
          </main>
          <footer />
        </div>
    );
  }
}

export default App;
