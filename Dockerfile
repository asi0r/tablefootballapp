FROM node:8.11-slim
EXPOSE 5000
WORKDIR .

RUN npm install -g serve

COPY package*.json ./

RUN npm install --silent

COPY . .

RUN npm run build

ENTRYPOINT ["serve", "-s", "build"]
